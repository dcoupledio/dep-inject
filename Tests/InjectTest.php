<?php 

require '../vendor/autoload.php';

use phpunit\framework\TestCase;
use Decoupled\Core\DependencyInjection\DependencyInjectionInvoker;
use Decoupled\Core\Application\ApplicationContainer;
use Decoupled\Core\Inflector\Inflector;
use Decoupled\Core\Action\Action;

class InjectTest extends TestCase{

    public function testCanInjectAction()
    {
        $app = new ApplicationContainer();

        $app['example'] = function(){

            return 1;
        };

        $invoker = new DependencyInjectionInvoker();

        $invoker->setApp( $app );

        $invoker->setInflector( new Inflector() );

        $action = new Action(function( $example ){

            return $example;
        });

        $action->setInvoker( $invoker );

        $this->assertEquals( $app['example'], $action() );

        return $action;
    }

    /**
     * @depends testCanInjectAction
     */

    public function testCanOverrideServiceValue( $action )
    {
        $this->assertEquals( $action(['example' => 2]), 2 );
    }
}