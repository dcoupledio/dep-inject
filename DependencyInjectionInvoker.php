<?php namespace Decoupled\Core\DependencyInjection;

use Decoupled\Core\Application\ApplicationContainer;
use Decoupled\Core\Action\ActionInvokerInterface;
use Decoupled\Core\DependencyInjection\UndefinedDependencyException;
use Decoupled\Core\Inflector\InflectorInterface;
use Decoupled\Core\Action\ActionInterface;

class DependencyInjectionInvoker implements ActionInvokerInterface{

	protected $app;

	protected $inflector;

	public function setApp( ApplicationContainer $app )
	{
		$this->app = $app;

		return $this;
	}

	public function getApp()
	{
		return $this->app;
	}

	public function getInflector()
	{
		return $this->inflector;
	}

	public function setInflector( InflectorInterface $inflector )
	{
		$this->inflector = $inflector;

		return $this;
	}

	public function resolveDependencies( array $deps, array $params = [] )
	{
		$resolved = [];

		$app = $this->getApp();

		$inflect = $this->getInflector();

		foreach( $deps as $dep )
		{
			//set inflector with dep name
			$name = $inflect( $dep );

			try
			{
				//use default params if there is a name match
				//name is inflected as varialbe when param passed

				if( isset($params[ $name->asVar() ]) )
				{
					$resolved[] = $params[ $name->asVar() ];
				}

				//if no param is given attempt to extract from
				//service container
				//inflect name as key/id

				else
				{
					$resolved[] = $app[ $name->asKey() ];
				}
			}
			catch( \Exception $e )
			{
				throw new UndefinedDependencyException(
					"Attempted to request undefined provider or parameter ".$name->asKey()
				);
			}
		}

		return $resolved;
	}

	public function __invoke( Action $action, array $params = [] )
	{
		return $this->invoke( $action, $params );
	}

	public function invoke( ActionInterface $action, array $params = [] )
	{
		$params = $this->resolveDependencies( 
			$action->getDeps(),
			$params 
		);

		$callable = $action->getCallable();

		return call_user_func_array( $callable, $params );
	}

}